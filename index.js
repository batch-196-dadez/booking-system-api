const express = require("express");

const mongoose = require("mongoose");
const app = express();
const port = 4000;



mongoose.connect("mongodb+srv://admin:admin123@cluster0.u2y5eb2.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{

	useNewUrlParser: true,
	useUnifiedTopology: true

});


let db = mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB Connection Error."));

db.once('open',()=>console.log("Connected to MongoDB."))


app.use(express.json());


const courseRoutes = require('./routes/courseRoutes');
const userRoutes = require('./routes/userRoutes');

app.use('/courses',courseRoutes);
app.use('/users',userRoutes);




app.listen(port,()=>console.log("ExpressJS API running at localost:4000"))
