/*
	TO be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

	the Router() method,will us to contain our routes
*/

const express = require("express");
const router = express.Router();

//Import the Course model so we can manipulate it and add a new course document.
const Course = require("../models/Course");

//All routes to courses now has an endpoint prefaced with /courses
//endpoint - /courses/
router.get('/',(req,res)=>{

	res.send("This route will get all course ");

});

//endpoint /courses/
router.post('/',(req,res)=>{

	//console.log(req.body);
	//Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model, and add methods for document creation.

	let newCourse = new Course({
	name: req.body.name,
	description: req.body.description,
	price: req.body.price

	})
	// console.log(newCourse)

	newCourse.save()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	.then(result => res.send(result))

	// .catch() - catches the errors and allows us to prcess and send to client
	.catch(error => res.send("Invalid input!"))

});

module.exports = router;
