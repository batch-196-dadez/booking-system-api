const express = require("express");
const router = express.Router();

const User = require("../models/User");

router.get('/',(req,res)=>{

	res.send("This route will get all users");

});

//endpoint /user/
router.post('/',(req,res)=>{

	//console.log(req.body);
	
	let newUser = new User({

	firstName: req.body.firstName,
	lastName: req.body.lastName,
	email: req.body.email,
        password: req.body.password,
        mobileNo: req.body.mobileNo

	})

	newUser.save()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	.then(result => res.send(result))


});

module.exports = router;
